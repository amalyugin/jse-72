package ru.t1.malyugin.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.malyugin.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_role")
public class Role {

    private static final long serialVersionUID = 1;

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.GUEST;

    @Override
    public String toString() {
        return roleType.getDisplayName();
    }

}