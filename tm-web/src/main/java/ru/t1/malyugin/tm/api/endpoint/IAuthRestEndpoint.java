package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.t1.malyugin.tm.model.Result;
import ru.t1.malyugin.tm.model.User;

@RequestMapping("/api/auth")
public interface IAuthRestEndpoint {

    @PostMapping("/login")
    Result login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    );

    @PostMapping("/logout")
    Result logout();

    @GetMapping(value = "/profile", produces = "application/json")
    User profile();

    @GetMapping(value = "/session", produces = "application/json")
    Authentication session();

}