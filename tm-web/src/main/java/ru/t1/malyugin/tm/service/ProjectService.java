package ru.t1.malyugin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.Random;

@Service
public class ProjectService implements IProjectService {

    private final Random random = new Random();
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Collection<Project> findAllForUser(final String userId) {
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public long countForUser(final String userId) {
        return projectRepository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void createForUser(final String userId) {
        int randomNumber = random.nextInt(101);
        Project project = new Project("P " + randomNumber, "DESC");
        project.setUserId(userId);
        projectRepository.saveAndFlush(project);
    }

    @Override
    @Transactional
    public void addForUser(final String userId, final Project project) {
        System.out.println("LOG SERVICE ADDFORUSER: " + project.getId());
        project.setUserId(userId);
        projectRepository.saveAndFlush(project);
    }

    @Override
    @Transactional
    public void deleteByIdForUser(final String userId, final String id) {
        if (!projectRepository.existsByUserIdAndId(userId, id)) return;
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void clearForUser(final String userId) {
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void editForUser(final String userId, final Project project) {
        if (!projectRepository.existsByUserIdAndId(userId, project.getId())) return;
        projectRepository.saveAndFlush(project);
    }

    @Override
    public Project findByIdForUser(final String userId, final String id) {
        return projectRepository.findByIdAndUserId(id, userId).orElse(null);
    }

}