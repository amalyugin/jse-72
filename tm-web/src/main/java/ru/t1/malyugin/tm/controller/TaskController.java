package ru.t1.malyugin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.CustomUser;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;

import java.util.Collection;
import java.util.Random;

@Controller
public class TaskController {

    private final Random random = new Random();

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/tasks")
    public String showTaskList(
            @AuthenticationPrincipal final CustomUser user,
            final Model model
    ) {
        Collection<Task> tasks = taskService.findAllForUser(user.getUserId());
        model.addAttribute("tasks", tasks);
        return "list/taskList";
    }

    @GetMapping("/task/create")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        taskService.createForUser(user.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.deleteByIdForUser(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") Task task
    ) {
        taskService.editForUser(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id,
            Model model
    ) {
        final Task task = taskService.findByIdForUser(user.getUserId(), id);
        model.addAttribute("task", task);

        final Collection<Project> projects = projectService.findAllForUser(user.getUserId());
        model.addAttribute("projects", projects);

        model.addAttribute("statuses", Status.values());

        return "/edit/taskEdit";
    }

}