package ru.t1.malyugin.tm.unit.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.configuration.WebApplicationConfiguration;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.repository.TaskRepository;
import ru.t1.malyugin.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class})
public class TaskRepositoryTest {

    private static final String USERNAME = "unitTester";
    private static final String PASSWORD = "unitTester";
    private final Task task1 = new Task("T1", "T1");
    private final Task task2 = new Task("T2", "T2");
    private final Task task3 = new Task("T3", "T3");
    private final Task task4 = new Task("T4", "T4");
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        task4.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    public void clean() {
        taskRepository.deleteByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdTest() {
        final Task task = taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()).orElse(null);
        Assert.assertEquals(task1.getId(), task.getId());
        Assert.assertEquals(task1.getName(), task.getName());
        Assert.assertEquals(task1.getDescription(), task.getDescription());
        Assert.assertEquals(task1.getStatus(), task.getStatus());
    }

    @Test
    public void deleteAllByUserIdTest() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskRepository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteByIdTest() {
        Assert.assertNotNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()).orElse(null));
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()).orElse(null));
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(taskRepository.existsByUserIdAndId(UserUtil.getUserId(), task1.getId()));
        Assert.assertFalse(taskRepository.existsByUserIdAndId(UserUtil.getUserId(), task3.getId()));
    }

    @Test
    public void countByUserTest() {
        Assert.assertEquals(2, taskRepository.countByUserId(UserUtil.getUserId()));
    }

}