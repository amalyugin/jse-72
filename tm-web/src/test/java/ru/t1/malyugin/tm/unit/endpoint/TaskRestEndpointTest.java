package ru.t1.malyugin.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.malyugin.tm.configuration.WebApplicationConfiguration;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Task;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class})
public class TaskRestEndpointTest {

    private static final String BASE_URL = "http://localhost:8080/api/task/";
    private static final String USERNAME = "unitTester";
    private static final String PASSWORD = "unitTester";
    private final Task task1 = new Task("P1", "P1");
    private final Task task2 = new Task("P2", "P2");
    private final Task task3 = new Task("P3", "P3");
    private final Task task4 = new Task("P4", "P4");
    @Autowired
    private AuthenticationManager authenticationManager;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext wac;

    @SneakyThrows
    public void saveTask(final Task task) {
        final String url = BASE_URL + "post";
        String taskJson = new ObjectMapper()
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(task);

        mockMvc.perform(MockMvcRequestBuilders.post(url).content(taskJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    public Task findById(final String id) {
        final String url = BASE_URL + "get/" + id;
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println("JSON: " + json);
        if ("".equals(json)) return null;
        final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Task.class);
    }

    @SneakyThrows
    public List<Task> findAll() {
        final String url = BASE_URL + "getAll";
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Task[].class));
    }

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        saveTask(task1);
        saveTask(task2);
    }

    @After
    @SneakyThrows
    public void clean() {
        final String url = BASE_URL + "delete/all";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getByIdTest() {
        saveTask(task3);
        final Task task = findById(task3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task3.getId(), task.getId());
    }

    @Test
    public void createTest() {
        saveTask(task3);
        final Task task = findById(task3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task3.getId(), task.getId());
    }

    @Test
    public void getListTest() {
        Assert.assertEquals(2, findAll().size());
    }

    @Test
    @SneakyThrows
    public void countTest() {
        final String url = BASE_URL + "count";
        final String count = mockMvc.perform(MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        Assert.assertEquals(new Long(2), new Long(count));
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        final String url = BASE_URL + "delete/all";
        mockMvc.perform(MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, findAll().size());
    }

}