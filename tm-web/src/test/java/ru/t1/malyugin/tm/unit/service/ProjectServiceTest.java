package ru.t1.malyugin.tm.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.configuration.WebApplicationConfiguration;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.UserUtil;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class})
public class ProjectServiceTest {

    private static final String USERNAME = "unitTester";
    private static final String PASSWORD = "unitTester";
    private final Project project1 = new Project("P1", "P1");
    private final Project project2 = new Project("P2", "P2");
    @Autowired
    private IProjectService projectService;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectService.addForUser(UserUtil.getUserId(), project1);
        projectService.addForUser(UserUtil.getUserId(), project2);
    }

    @After
    public void clean() {
        projectService.clearForUser(UserUtil.getUserId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectService.countForUser(UserUtil.getUserId()));
    }

    @Test
    public void findByIdTest() {
        final Project project = projectService.findByIdForUser(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project1.getId(), project.getId());
        Assert.assertEquals(project1.getName(), project.getName());
        Assert.assertEquals(project1.getDescription(), project.getDescription());
        Assert.assertEquals(project1.getStatus(), project.getStatus());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, projectService.countForUser(UserUtil.getUserId()));
    }

    @Test
    public void deleteAll() {
        projectService.clearForUser(UserUtil.getUserId());
        Assert.assertEquals(0, projectService.countForUser(UserUtil.getUserId()));
    }

    public void deleteById() {
        Assert.assertNotNull(projectService.findByIdForUser(UserUtil.getUserId(), project1.getId()));
        projectService.deleteByIdForUser(UserUtil.getUserId(), project1.getId());
        Assert.assertNull(projectService.findByIdForUser(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    public void editTest() {
        final Project project = projectService.findByIdForUser(UserUtil.getUserId(), project1.getId());
        project.setDescription("NEW D");
        project.setName("NEW N");
        projectService.editForUser(UserUtil.getUserId(), project);
        final Project newProject = projectService.findByIdForUser(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project.getId(), newProject.getId());
        Assert.assertEquals(project.getName(), "NEW N");
        Assert.assertEquals(project.getDescription(), "NEW D");
    }

}