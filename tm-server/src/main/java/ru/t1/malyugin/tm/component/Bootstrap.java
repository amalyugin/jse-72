package ru.t1.malyugin.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.service.ILoggerService;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.endpoint.AbstractEndpoint;
import ru.t1.malyugin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] abstractEndpoints;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initEndpoints(@NotNull final AbstractEndpoint[] endpoints) {
        Arrays.stream(endpoints).forEach(this::registryEndpoint);
    }

    private void registryEndpoint(@Nullable final AbstractEndpoint endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();

        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void prepareStartup() {
        BasicConfigurator.configure();
        initPID();
        initEndpoints(abstractEndpoints);
        @NotNull final String applicationName = propertyService.getApplicationName();
        loggerService.info("** WELCOME TO " + applicationName + " **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TM SERVER IS SHUTTING DOWN **");
    }

    public void run(@Nullable final String... args) {
        prepareStartup();
    }

}