package ru.t1.malyugin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "user-update";

    @NotNull
    private static final String DESCRIPTION = "update current user profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE USER PROFILE]");

        System.out.print("ENTER FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.print("ENTER MIDDLE NAME: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.print("ENTER LAST NAME: ");
        @NotNull final String lastName = TerminalUtil.nextLine();

        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken(), firstName, middleName, lastName);
        userEndpoint.updateProfile(request);
    }

}