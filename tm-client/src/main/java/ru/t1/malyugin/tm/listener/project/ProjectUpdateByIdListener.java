package ru.t1.malyugin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.project.ProjectUpdateByIdRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-update-by-id";

    @NotNull
    private static final String DESCRIPTION = "Update project by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectUpdateByIdListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE PROJECT BY ID]");

        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NEW NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken(), id, name, description);
        projectEndpoint.updateProjectById(request);
    }

}