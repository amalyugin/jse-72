package ru.t1.malyugin.tm.event;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class ConsoleEvent {

    @NotNull
    private final String message;

    public ConsoleEvent(@NotNull final String message) {
        this.message = message;
    }

}